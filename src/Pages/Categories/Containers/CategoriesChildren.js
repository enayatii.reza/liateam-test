import React from 'react';
import { connect } from 'react-redux';
import { setCategoryId } from '../../../reducers/products';
import { withRouter } from "react-router-dom";
import CategoryButton from "../Components/CategoryButton";

class CategoriesChildren extends React.Component {
  constructor(props){
    super(props);
    this.clickCategory = categoryId => () => {
      this.props.saveCategoryId(categoryId);
      this.props.history.push(`/products/?category=${categoryId}`)
    }
  }
  render(){
    const renderItems = this.props.selectedCategories.children.map((value, index) => 
        <div key={index} onClick={this.clickCategory(value.id)}>
            <CategoryButton title={value.name} image={this.props.selectedRootCategory.image} />
        </div>
    )
    return (
      <div dir='rtl'>
          {renderItems}
      </div>
    );
  }
}

const mapStateToProps = state => ({
    ...state,
    selectedRootCategory: state.categories.selectedRootCategory
});

const mapDispatchToProps = dispatch => ({
  saveCategoryId: id => dispatch(setCategoryId(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CategoriesChildren));
