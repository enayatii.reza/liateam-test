import React from 'react';
import Main from '../../Layouts/AppMain';
import { connect } from 'react-redux';
import CategoriesChildren from './Containers/CategoriesChildren';
import { withRouter } from 'react-router-dom';

class Categories extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            
        }
    }
    componentDidMount() {
    }

    render(){
        return (
            <Main>
                <div className='pt-5 container'>
                    <div className='text-lg-right'>دسته بندی</div>
                    <CategoriesChildren selectedCategories={this.props.selectedCategories} />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    selectedCategories: state.categories.selectedRootCategory,
    categoriesData: state.categories.categoriesData
});

const mapDispatchToProps = dispatch => ({
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Categories));

