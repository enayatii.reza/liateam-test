import React from 'react';
import helpers from '../../../utils/helpers';

class CategoryButton extends React.Component {
    render() {
        return (
            <div dir='rtl' className='d-flex justify-content-between category-button'>
                <img src={helpers.compeleteUrl(this.props.image)} className='category-image'/>
                <div className='m-auto'>{this.props.title}</div>
            </div>
        );
    };
};

export default CategoryButton;