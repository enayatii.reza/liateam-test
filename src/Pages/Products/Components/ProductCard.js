import React from 'react';
import helpers from '../../../utils/helpers';
import {setProductsData} from "../../../reducers/products";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {addProductToCart} from "../../../reducers/carts";

class ProductCard extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddToCart = product => () => this.props.saveProductToCart(product.id, product)
    }
    render() {
        return (
            <div dir='rtl' className='product-card text-center'>
                <img src={helpers.compeleteUrl(this.props.product.small_pic)} className='product-image' />
                <div className='' >{this.props.product.title}</div>
                <div className='color-red'>{this.props.product.volume}</div>
                <div className='d-flex justify-content-between'>
                    <div onClick={this.handleAddToCart(this.props.product)} className='btn buy-button p-2'>خرید</div>
                    <div className='row p-2 price-holder float-left'>
                        <div className='price'>{this.props.product.price.price}</div>
                        <div className='mt-auto mb-auto toman'>{'تومان'}</div>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = state => ({
    ...state,
    cartsProducts: state.carts.products
});

const mapDispatchToProps = dispatch => ({
    saveProductToCart: (id, data) => dispatch(addProductToCart(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);