import React from 'react';
import Main from '../../Layouts/AppMain';
import { getProducts } from '../../Services/api';
import { connect } from 'react-redux';
import {changePage, setPaginationInfo, setProductsData} from '../../reducers/products';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter } from 'react-router-dom';
import helpers from '../../utils/helpers';
import ProductCard from "./Components/ProductCard";
import Pagination from '@material-ui/lab/Pagination';

class Products extends React.Component {
    constructor(props){
        super(props);
        this.refresh = (page) => {
            const category = helpers.parseQueryParams(window.location.search).category
            console.log(category)
            getProducts(
                category,
                page
            )
                .then(response => {
                    let pos = -1;
                    let data = [[],[],[]];
                    response.list.forEach((product, index) => {
                        if(index%4 === 0)
                            pos++;
                        data[pos][index%4] = product;
                    })
                    this.props.saveProductsData(data);
                    this.props.savePaginationInfo(response.page, response.pagecount, response.totalcount);
                })
                .catch(error => console.error(error))
        }
    }
    componentDidMount(){
        this.refresh(1);
    }
    render(){
        const renderItems = this.props.productsData.map((value, index) => 
            <div className='row' key={index}>
                {value.map((product, i) =>
                    <div key={i} className='col-md'>
                        <ProductCard product={value[i]} />
                    </div>
                )}
            </div>
        );
        return (
            <Main>
                <div className='container' dir='rtl'>
                    {renderItems}
                </div>
                <div className='mt-4 d-flex justify-content-center'>
                    <Pagination
                        color={"secondary"}
                        onChange={(event, page) => {
                            this.refresh(page);
                            console.log(page)
                        }}
                        count={this.props.paginationInfo.pageCount} />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    productsData: state.products.productsData,
    paginationInfo: state.products.pagination
});

const mapDispatchToProps = dispatch => ({
    saveProductsData: (data) => dispatch(setProductsData(data)),
    savePaginationInfo: (page, pageCount, totalCount) => dispatch(setPaginationInfo(page, pageCount, totalCount)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Products));

