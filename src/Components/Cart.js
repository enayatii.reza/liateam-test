import React from 'react';
import helpers from "../utils/helpers";

class Cart extends React.Component{
    render() {
        const renderItems = Object.entries(this.props.items).map((item, index) =>
            <div className='cart-item'>
                <div className='cart-item-inner justify-content-between'>
                    <div className='cart-item-image-container'>
                        <img className='cart-item-image' src={helpers.compeleteUrl(item[1].data.small_pic)} />
                    </div>
                    <div className='cart-item-content'>
                        <div>{item[1].data.title}</div>
                        <div>{item[1].data.price.price}</div>
                        <div className='quantity-container'>
                            <div onClick={this.props.increaseProduct(item[0], item[1].data)} className='btn text-white'>+</div>
                            <div className='m-auto'>{item[1].quantity}</div>
                            <div onClick={() => {
                                if (item[1].quantity !== 1)
                                    this.props.decreaseProduct(item[0], item[1].data)
                            }} className='btn text-white'>-</div>
                        </div>
                    </div>
                    <div onClick={this.props.removeProduct(item[0], item[1].data)} className='remove-product-btn btn text-white p-0'>حذف</div>
                </div>
            </div>
        )
        return (
            <div className='cart-container'>
                <div className='text-white'>
                    {renderItems}
                    <div className='sum-price-container'>
                        <div>جمع کل:</div>
                        <div>{this.props.sumPrice}</div>
                    </div>
                    <div className='p-3'>
                        <div className='cart-submit-btn text-center'>ثبت سفارش</div>
                    </div>
                </div>
            </div>
        );
    };
};

export default Cart;