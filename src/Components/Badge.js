import React from 'react';

export class Badge extends React.PureComponent{
    render() {
        return (
            <div>
                {this.props.badgeContent === 0 &&
                    <div>
                        {this.props.children}
                    </div>
                }
                {this.props.badgeContent !== 0 &&
                    <div className='badge-cart'>
                        {this.props.badgeContent}
                        {this.props.children}
                    </div>
                }
            </div>

        );
    };
};