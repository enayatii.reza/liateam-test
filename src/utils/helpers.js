const compeleteUrl = url => {
    return 'http://back.dev.liateam.ir' + url
}

const parseQueryParams = search => {
    if (search) {
        search = search.substring(1);
        return  JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
    } else {
        return 1
    }
}

module.exports = {
    compeleteUrl,
    parseQueryParams
}