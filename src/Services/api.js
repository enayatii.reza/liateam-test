import Axios from "axios";

// const mainUrl = process.env.REACT_APP_BASE_URL;

export const getCategoriesList = async () => {
    return new Promise((resolve, reject) => {
        Axios({
            url: 'http://back.dev.liateam.ir/api/rest/v1/get_categories',
            method: 'GET'
        })
            .then(response => resolve(response.data))
            .catch((error) => reject(error))
    });
}

export const getProducts = async (categories, page) => {
    return new Promise((resolve, reject) => {
        Axios({
            url: 'http://back.dev.liateam.ir/api/rest/v1/get_product',
            method: 'GET',
            params: {
                categories: categories,
                page: page
            }
        })
            .then(response => resolve(response.data))
            .catch((error) => reject(error))
    });
}