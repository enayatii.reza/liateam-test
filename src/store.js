import {createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {composeWithDevTools} from 'redux-devtools-extension';
import storage from 'redux-persist/lib/storage'
import reducer from './reducer';

const persistConfig = {
    storage,

    key: 'root',

    // Whitelist (Save Specific Reducers)
    whitelist: [
        'auth',
        // 'themeConfig'
    ],

    // Blacklist (Don't Save Specific Reducers)
    blacklist: [],
};
const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
    persistedReducer, composeWithDevTools()
);
let persistor = persistStore(store);
export {
    store,
    persistor,
};