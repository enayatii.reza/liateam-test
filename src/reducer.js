import {routerReducer} from 'react-router-redux';
import {combineReducers} from 'redux';
import categories from './reducers/categories';
import products from './reducers/products';
import carts from "./reducers/carts";

const testRedux = (state = {}, action) => {
    switch (action.type) {
        case 'TESTING':
            return {
                ...state,
                logedIn: "true",
            };
        default:
            return state;
    }
}

export default combineReducers({
    testRedux,
    categories: categories,
    products: products,
    carts: carts,
    router: routerReducer,
});