export default (
    state = {
        categoryId: 0,
        productsData: [],
        pagination: {
            page: 1,
            pageCount: 1,
            totalCount: 1
        }
    },
    action) => {
    switch (action.type) {
        case 'SET_CATEGORY_ID':
            return {
                ...state,
                categoryId: action.categoryId
            };
        case 'SET_PRODUCTS_DATA':
            return {
                ...state,
                productsData: action.productsData
            };
        case 'SET_PAGINATION_INFO':
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    page: action.page,
                    pageCount: action.pageCount,
                    totalCount: action.totalCount
                }
            }
        default:
            return state;
    }
}

//actions
export const setProductsData = productsData => ({type: 'SET_PRODUCTS_DATA', productsData})
export const setCategoryId = categoryId => ({type: 'SET_CATEGORY_ID', categoryId})
export const setPaginationInfo = (page, pageCount, totalCount) => ({type: 'SET_PAGINATION_INFO', page, pageCount, totalCount})
