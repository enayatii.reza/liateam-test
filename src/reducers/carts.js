export default (
    state = {
        products: {},
        quantity: 0,
        sumPrice: 0
        /** products:
         *      {
         *          productsID : {
         *              data:
         *              quantity
         *          }
         *      }
         *
         */
    },
    action) => {
    switch (action.type) {
        case 'ADD_PRODUCT_TO_CART': {
            let products = {...state.products};
            const newProducts = Object.assign({},products)
            if (action.id in products) {
                newProducts[action.id].quantity++;
            }
            else
                newProducts[action.id] = {
                    data: action.data,
                    quantity: 1
                }
            return {
                ...state,
                products: newProducts,
                quantity: state.quantity + 1,
                sumPrice: state.sumPrice + action.data.price.final_price
            };
        }
        case 'REMOVE_PRODUCT_FROM_CART': {
            let products = {...state.products};
            const newProducts = Object.assign({},products)
            const quantity = newProducts[action.id].quantity;
            delete newProducts[action.id]
            return {
                ...state,
                products: newProducts,
                quantity: state.quantity - quantity,
                sumPrice: state.sumPrice - action.data.price.final_price * quantity
            };
        }
        case 'DECREASE_PRODUCT_IN_CART': {
            let products = {...state.products};
            const newProducts = Object.assign({},products);
            newProducts[action.id].quantity--;
            return {
                ...state,
                products: newProducts,
                quantity: state.quantity - 1,
                sumPrice: state.sumPrice - action.data.price.final_price
            }
        }
        case 'INCREASE_PRODUCT_IN_CART': {
            let products = {...state.products};
            const newProducts = Object.assign({},products);
            newProducts[action.id].quantity++;
            return {
                ...state,
                products: newProducts,
                quantity: state.quantity + 1,
                sumPrice: state.sumPrice + action.data.price.final_price
            }
        }
        default:
            return state;
    }
}

//actions
export const addProductToCart = (id, data) => ({type: 'ADD_PRODUCT_TO_CART', id, data})
export const increaseProductToCart = (id, data) => ({type: 'INCREASE_PRODUCT_IN_CART', id, data})
export const decreaseProductToCart = (id, data) => ({type: 'DECREASE_PRODUCT_IN_CART', id, data})
export const removeProductToCart = (id, data) => ({type: 'REMOVE_PRODUCT_FROM_CART', id, data})



