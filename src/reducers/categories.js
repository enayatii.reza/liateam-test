export default (
    state = {
        categoriesData: [],
        selectedRootCategory: {
            children: []
        }
    },
    action) => {
    switch (action.type) {
        case 'SET_CATEGORIES_DATA':
            return {
                ...state,
                categoriesData: action.categoriesData
            };
        case 'SET_SELECTED_CATEGORY':
            return {
                ...state,
                selectedRootCategory: action.selectedRootCategory
            }
        default:
            return state;
    }
}

//actions
export const setCategoriesData = categoriesData => ({type: 'SET_CATEGORIES_DATA', categoriesData})
export const setSelectedRootCategory = selectedRootCategory => ({type: 'SET_SELECTED_CATEGORY', selectedRootCategory})