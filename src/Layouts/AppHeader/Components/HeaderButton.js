import React from 'react';

class HeaderButton extends React.Component {
  render(){
    return (
      <div className='btn pt-2 pb-2 pr-4 pl-4'>
        <div className='header-button'>
          {this.props.title}
        </div>
      </div>
    );
  }
}

export default HeaderButton;
