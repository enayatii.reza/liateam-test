import React from 'react';

class SubHeaderButton extends React.Component {
    render(){
        return (
            <div className='btn pt-2 pb-1 pr-3 pl-3'>
                <button className='sub-header-button mr-3'>
                    {this.props.title}
                </button>
            </div>
        );
    }
}

export default SubHeaderButton;
