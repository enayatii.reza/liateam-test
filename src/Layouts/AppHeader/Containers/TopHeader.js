import React from 'react';
import Logo from '../../../assets/images/HeaderLogo.png';
import HeaderButton from "../Components/HeaderButton";

class TopHeader extends React.Component {
  render(){
    return (
      <header dir='rtl' className='d-flex justify-content-between'>
            <div className='row d-flex'>
              <div className='p-2 mr-4'>
                <img style={{height: '75px'}} src={Logo} className='p-2'/>
              </div>
              <nav className='navbar'>
                <HeaderButton title={'خانه'} />
                <HeaderButton title={'وبلاگ'} />
                <HeaderButton title={'درباره ی ما'} />
                <HeaderButton title={'تیم اجرایی'} />
                <HeaderButton title={'تماس با ما'} />
                <HeaderButton title={'سوالات متداول'} />
                <HeaderButton title={'پورتال آموزشی'} />
              </nav>
            </div>
            <nav className='navbar'>
              <HeaderButton title={'پروفایل'} />
            </nav>

      </header>
    );
  }
}

export default TopHeader;
