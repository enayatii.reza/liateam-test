import React from 'react';
import { connect } from 'react-redux';
import { setSelectedRootCategory } from '../../../reducers/categories';
import SubHeaderButton from "../Components/SubHeaderButton";
import { withRouter } from "react-router-dom";
import {Badge} from "../../../Components/Badge";
import Cart from "../../../Components/Cart";
import {decreaseProductToCart, increaseProductToCart, removeProductToCart} from "../../../reducers/carts";

class SubHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openCart: false
        };
        this.clickRootCategory = value => () => {
            this.props.history.push(`/category/`)
            this.props.saveSelectedCategory(value)
        }
        this.clickCart = () => this.setState({openCart: !this.state.open})
        this.increaseProduct = (id, product) =>  () => this.props.increaseProductsInCart(id, product)
        this.decreaseProduct = (id, product) => this.props.decreaseProductsInCart(id, product)
        this.removeProduct = (id, product) =>  () => this.props.removeProductInCart(id, product)
    }
    render(){
        const renderItems = this.props.rootCategories.map((value, index) =>
            <div key={index} onClick={this.clickRootCategory(value)}>
                <SubHeaderButton title={value.name} />
            </div>
        )
        return (
            <header dir='rtl' className='d-flex justify-content-between'>
                <nav className='navbar'>
                    {renderItems}
                </nav>
                <nav className='navbar ml-5'>
                    <Badge badgeContent={this.props.cartQuantity}>
                        <div onClick={this.clickCart} className='btn p-0' style={{width: 'max-content'}} >سبد خرید</div>
                    </Badge>
                    <div>
                        {this.state.openCart &&
                            <Cart
                                increaseProduct={this.increaseProduct}
                                decreaseProduct={this.decreaseProduct}
                                removeProduct={this.removeProduct}
                                sumPrice={this.props.sum}
                                items={this.props.cartProduct} /> }
                    </div>
                </nav>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    categoriesData: state.categories.categoriesData,
    cartQuantity: state.carts.quantity,
    cartProduct: state.carts.products,
    sum: state.carts.sumPrice
});

const mapDispatchToProps = dispatch => ({
    saveSelectedCategory: (data) => dispatch(setSelectedRootCategory(data)),
    increaseProductsInCart: (id, product) => dispatch(increaseProductToCart(id, product)),
    decreaseProductsInCart: (id, product) => dispatch(decreaseProductToCart(id, product)),
    removeProductInCart: (id, product) => dispatch(removeProductToCart(id, product)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SubHeader));