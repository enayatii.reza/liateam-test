import React from 'react';
import SubHeader from "./Containers/SubHeader";
import TopHeader from "./Containers/TopHeader";

class Header extends React.Component {
    render() {
        return (
            <div className=''>
                <TopHeader />
                <div className='header-line'></div>
                <SubHeader rootCategories={this.props.categoriesData} />
            </div>
        );
    };
};

export default Header;