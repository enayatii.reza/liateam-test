import React from 'react';
import Footer from './AppFooter';
import {getCategoriesList} from "../Services/api";
import {setCategoriesData} from "../reducers/categories";
import {connect} from "react-redux";
import Header from "./AppHeader";

class Main extends React.Component {
    componentDidMount(){
        getCategoriesList()
            .then(response => this.props.saveCategoriesData(response))
            .catch(error => console.error(error))
    }
      render(){
        return (
          <div>
                <Header categoriesData={this.props.categoriesData} />
                <div>
                    {this.props.children}
                </div>
                <Footer />
          </div>
        );
      }
}

const mapStateToProps = state => ({
    ...state,
    categoriesData: state.categories.categoriesData,
    selectedCategories: state.categories.selectedRootCategory
});

const mapDispatchToProps = dispatch => ({
    saveCategoriesData: (data) => dispatch(setCategoriesData(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
