import React, { Fragment } from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
// import {connect} from 'react-redux';
import Categories from './Pages/Categories';
import Products from './Pages/Products';

class App extends React.Component {
  render(){
    return (
      <div>
        <Router basename='/'>
            <Fragment>
                <Switch>
                    <Route exact  path='/' component={Products} />
                    <Route exact  path='/category' component={Categories}/>
                    <Route exact  path='/products' component={Products}/>
                </Switch>
            </Fragment>
        </Router>
      </div>
      );
  }
}

export default App;
